import React from "react";
import "./App.css";

function App(props: { message: string; logo: { value: string } }) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={props.logo.value} className="App-logo" alt="logo" />
        <p>{props.message}</p>
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
